function mainMenuUpdate() {
  pointerAnim.y = 115+(menuLogic.cursorPos*40);

  if(GAMEPAD||false) {
    if(Date.now()-NOTPRELL>200) {
      if (GAMEPAD.up||GAMEPAD.buttons[12].pressed) {
        menuLogic.cursorPos = (menuLogic.cursorPos<=0)? menuLogic.cursorPosMax : menuLogic.cursorPos-1;
        NOTPRELL = Date.now();
      } else if (GAMEPAD.down||GAMEPAD.buttons[13].pressed) {
        menuLogic.cursorPos = (menuLogic.cursorPos>=menuLogic.cursorPosMax)? 0 : menuLogic.cursorPos+1;
        NOTPRELL = Date.now();
      } 
    } 
    if (GAMEPAD.A) {
      if(menuLogic.cursorPos === 0) {
        this.scene.start("mainGame");
      }
    }
  }
};