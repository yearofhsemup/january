function mainMenuCreate() {

  if(!fireballAnim) {
    animConfigs.fireball.frames = this.anims.generateFrameNumbers('bigFireball');
    fireballAnim = this.anims.create(animConfigs.fireball);
  }
  pointerAnim = this.add.sprite(330,115, 'bigFireball').setInteractive();
  pointerAnim.flipX = true;
  pointerAnim.anims.load('fireballFly');
  pointerAnim.anims.play('fireballFly');

  menuText.start = this.add.text(350, 100, 'start', { fontSize: '24px', fill: '#FFF' }).setInteractive();
  menuText.options = this.add.text(350, 140, 'options', { fontSize: '24px', fill: '#FFF' }).setInteractive();
  menuText.info = this.add.text(350, 180, 'info', { fontSize: '24px', fill: '#FFF' }).setInteractive();
  // menuText.gamepad = this.add.text(32, config.height-23, 'press [A] button to select gamepad', { fontSize: '18px', fill: '#FFF' }).setInteractive();


  // start game 
  // pointerAnim.on('pointerdown', pointer => { this.scene.start("mainGame"); });
  
  menuText.start.on('pointerdown', pointer => { this.scene.start("mainGame"); });

  this.input.gamepad.once('down', function(pad, button, index) {
    // menuText.gamepad.setText('Gamepad selected '+pad.id.substr(0,23));
    GAMEPAD = pad;
  }, this);

  this.input.keyboard.on('keyup', event => {
    if(event.keyCode === 38) { // cursor up
      menuLogic.cursorPos = (menuLogic.cursorPos<=0)? menuLogic.cursorPosMax : menuLogic.cursorPos-1;
    } else if(event.keyCode === 40) { // cursor down
      menuLogic.cursorPos = (menuLogic.cursorPos>=menuLogic.cursorPosMax)? 0 : menuLogic.cursorPos+1;
    } else if(event.keyCode === 13 || event.keyCode === 32) { // enter or space
      if(menuLogic.cursorPos === 0) {
        this.scene.start("mainGame");
      }
    }
  });

  // global pause
  document.body.addEventListener('keyup', e => { 
    if(e.keyCode === 80) { // press p
      if(game.scene.isActive(HUD.activeScene)) {
        game.scene.pause(HUD.activeScene);
      } else {
        game.scene.resume(HUD.activeScene);
      }
    }
  });
}

let menuText = {
  start: {},
  options: {},
  info: {},
  gamepad: {}
};

let menuLogic = {
  cursorPos: 0,
  cursorPosMax: 2,
  deBounce: 0,
  deBounceTime: 150
};