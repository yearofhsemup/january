function mainMenuPreload() {
  this.load.spritesheet('fireball', 'assets/sprites/Jan_Fireball.png', { frameWidth: 16, frameHeight: 8 });
  this.load.spritesheet('bigFireball', 'assets/sprites/Jan_big-Fireball.png', { frameWidth: 32, frameHeight: 16 });
}

// function mainMenuInitialize() {
//   Phaser.Scene.call(this, { key: 'mainMenu' });
// }