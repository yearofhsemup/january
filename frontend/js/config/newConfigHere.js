animConfigs.fireball = {
  key: 'fireballFly',
  frames: 4,
  frameRate: 6,
  // yoyo: false,
  repeat: -1
};

animConfigs.bigFireball = {
  key: 'bigFireballFly',
  frames: 4,
  frameRate: 6,
  // yoyo: false,
  repeat: -1
};

animConfigs.ship = {
  key: 'shipFly',
  frames: createFramesFromTo('ship',0,4),
  frameRate: 16,
  // yoyo: false,
  repeat: -1
};

animConfigs.coin1 = {
  key: 'coinRotate1',
  frames: createFramesFromTo('coin123',0,9),
  frameRate: 8,
  repeat: -1
};

animConfigs.coin2 = {
  key: 'coinRotate2',
  frames: createFramesFromTo('coin123',10,19),
  frameRate: 8,
  hideOnComplete: true,
  repeat: -1
};

animConfigs.coin3 = {
  key: 'coinRotate3',
  frames: createFramesFromTo('coin123',20,29),
  frameRate: 8,
  hideOnComplete: true,
  repeat: -1
};

animConfigs.blendAsteroid01 = {
  key: 'blendAsteroid01Fly',
  frames: createFramesFromTo('blendAsteroid01',0,15),
  frameRate: 12,
  // yoyo: false,
  repeat: -1
};

animConfigs.blendAsteroid01Explode = {
  key: 'blendAsteroid01Explode',
  frames: createFramesFromTo('blendAsteroid01',16,27),
  frameRate: 16,
  // yoyo: false,
  hideOnComplete: true,
  repeat: 0
};

animConfigs.blendAsteroid02 = {
  key: 'blendAsteroid02Fly',
  frames: createFramesFromTo('blendAsteroid02',0,15),
  frameRate: 10,
  // yoyo: false,
  repeat: -1
};

animConfigs.blendAsteroid02Explode = {
  key: 'blendAsteroid02Explode',
  frames: createFramesFromTo('blendAsteroid02',16,25),
  frameRate: 16,
  // yoyo: false,
  hideOnComplete: true,
  repeat: 0
};


animConfigs.blendAsteroid03 = {
  key: 'blendAsteroid03Fly',
  frames: createFramesFromTo('blendAsteroid03',0,15),
  frameRate: 10,
  // yoyo: false,
  repeat: -1
};

animConfigs.blendAsteroid03Explode = {
  key: 'blendAsteroid03Explode',
  frames: createFramesFromTo('blendAsteroid03',16,24),
  frameRate: 16,
  // yoyo: false,
  hideOnComplete: true,
  repeat: 0
};

animConfigs.blendAsteroid04 = {
  key: 'blendAsteroid04Fly',
  frames: createFramesFromTo('blendAsteroid04',0,15),
  frameRate: 10,
  // yoyo: false,
  repeat: -1
};

animConfigs.blendAsteroid04Explode = {
  key: 'blendAsteroid04Explode',
  frames: createFramesFromTo('blendAsteroid04',16,23),
  frameRate: 16,
  // yoyo: false,
  hideOnComplete: true,
  repeat: 0
};

animConfigs.quadEvil02Down = {
  key: 'quadEvil02Down',
  frames: createFramesFromTo('quadEvil02',0,14),
  frameRate: 16,  
  repeat: -1
};

animConfigs.quadEvil02Right = {
  key: 'quadEvil02Right',
  frames: createFramesByList('quadEvil02',[15,16,17,18,19,20,21,22,23,24,25,26,14,14,14,14]),
  frameRate: 16,  
  repeat: -1
};

animConfigs.quadEvil02Left = {
  key: 'quadEvil02Left',
  frames: createFramesToFrom('quadEvil02',27,14),
  frameRate: 16,
  repeat: -1
}

animConfigs.quadEvil02Up = {
  key: 'quadEvil02Up',
  frames: createFramesToFrom('quadEvil02',14,0),
  frameRate: 16,  
  repeat: -1
};

let blendAsteroid01Anim = null;
let blendAsteroid01Explosion = null;
let blendAsteroid02Anim = null;
let blendAsteroid02Explosion = null;
let blendAsteroid03Anim = null;
let blendAsteroid03Explosion = null;
let blendAsteroid04Anim = null;
let blendAsteroid04Explosion = null;

let quadEvil02DownAnim = null;
let quadEvil02RightAnim = null;
let quadEvil02LeftAnim = null;
let quadEvil02UpAnim = null;

let blendAsteroidGroup = [];
let blendAsteroidExplosionGroup = [];

let enemyz = {
  quadEvil02: {},
  fireball01: [],
  fireball02: [],
};

let enemyWaveWaiter = [];
let enemyDealerState = {last:Date.now(),history:[],rndList:[]};