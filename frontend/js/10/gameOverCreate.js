let gameOverText = {};
let GAMEOVER = {
  mainGamePaused: false,
  mainGameStop: false,
};
const stopOverStartGame = () => {
  restPlayData();
  game.scene.stop("gameOver");
  game.scene.stop("mainGame");
  game.scene.start("mainMenu");
  GAMEOVER.mainGamePaused = false;
  GAMEOVER.mainGameStop = false;
  // game.scene.resume("mainGame");
};

function gameOverCreate() {
  HUD.activeScene = 'gameOver';
  this.scene.bringToTop('gameOver');
  gameOverText = this.add.text((config.width-320)/2, -32, 'GAME OVER', { fontSize: '64px', fill: '#FFF' }).setInteractive();

  gameOverText.on('pointerdown', pointer => { stopOverStartGame(); });

  this.input.gamepad.once('down', function(pad, button, index) {
    GAMEPAD = pad;
  }, this);
}