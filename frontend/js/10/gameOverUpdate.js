function gameOverUpdate() {
  if(gameOverText.y>=config.height/3 && !GAMEOVER.mainGamePaused) {
    this.scene.pause('mainGame');
    GAMEOVER.mainGamePaused = true;
  } else if(gameOverText.y<=config.height/2) {
    gameOverText.y += 1;
  } else {
    GAMEOVER.mainGameStop = true;
  }

  if(GAMEPAD||false) {
    if(GAMEPAD.A) {
      stopOverStartGame();
    }
  }
}