const bulletDealer = (index,type,x,y,vx,vy) => {
  if(type==="delete") {
    bulletGroup[index].sprite.visible=false;
    return;
  } else if(type==="add") {
    if(HUD.power<=0) return;
    HUD.power -= 0.01;
    for(let b in bulletGroup) {
      if(bulletGroup[b].sprite.visible===false) {
        bulletGroup[b].sprite.x = x || 0;
        bulletGroup[b].sprite.y = y || 0;
        bulletGroup[b].sprite.visible = true;
        bulletGroup[b].speedX = vx || 2;
        bulletGroup[b].speedY = vy || 0;
        break;
      }
    }
    return;
  }
};

const bonusDealer = (index,type,x,y,speed,rotation) => {
  if(type==="delete") {
    bonusGroup[index].sprite.visible=false;
  } else if(type==="add") {
    for(let b in bonusGroup) {
      if(bonusGroup[b].sprite.visible===false) {
        bonusGroup[b].sprite.x = x || config.width/2;
        bonusGroup[b].sprite.y = y || config.height/2;
        bonusGroup[b].sprite.visible=true;
        break;
      }
    }
  } else if(type==="coin") {
    // sInx = (index===0||index>=4)? 1 : index;
    // coinPool[sInx].sprite.x = x || config.width/2;
    // coinPool[sInx].sprite.y = y || config.height/2;
    // coinPool[sInx].sprite.visible = true;
    for(let c in coinPool) {
      if(coinPool[c].sprite.visible===false){
        coinPool[c].sprite.x = x || config.width/2;
        coinPool[c].sprite.y = y || config.height/2;
        coinPool[c].sprite.visible = true;
        coinPool[c].speed = (speed/4) || -0.23;
        break;
      }
    }
  }
};

const asteroidDealer = (index,type,x,y) => {
  if(type==="delete") {
    if(asteroidGroup[index].sprite.anims.currentFrame||false) asteroidGroup[index].sprite.anims.playReverse();
    asteroidGroup[index].sprite.visible=false;
  } else if(type==="add") {
    for(let a in asteroidGroup) {
      if(asteroidGroup[a].sprite.visible===false) {
        asteroidGroup[a].sprite.x = x || config.width/2;
        asteroidGroup[a].sprite.y = y || config.height/2;
        asteroidGroup[a].sprite.visible=true;
        asteroidGroup[a].playStart=false;
        asteroidGroup[a].power = parseInt(asteroidGroup[a].animKey.replace("explode0",""));
        break;
      }
    }
  }
};

const explosionDealer = (index,type,x,y,speed,rotation) => {
  if(type==="delete") {
    if(explosionGroup[index].sprite.anims.currentFrame||false) explosionGroup[index].sprite.anims.playReverse();
    explosionGroup[index].sprite.visible=false;
  } else if(type==="add") {
    for(let e in explosionGroup) {
      if(explosionGroup[e].sprite.visible===false) {
        explosionGroup[e].sprite.x = x || 23;
        explosionGroup[e].sprite.y = y || 23;
        explosionGroup[e].sprite.anims.play(explosionGroup[e].animKey);
        explosionGroup[e].sprite.visible = true;
        explosionGroup[e].speed = speed;
        explosionGroup[e].rotate = rotation;
        break;
      }
    }
  }
};

const magicExplosionDealer = (index,type,x,y,speed,rotation) => {
  if(type==="delete") {
    if(magicExplosionGroup[index].sprite.anims.currentFrame||false) magicExplosionGroup[index].sprite.anims.playReverse();
    magicExplosionGroup[index].sprite.visible=false;
  } else if(type==="add") {
    for(let e in magicExplosionGroup) {
      if(magicExplosionGroup[e].sprite.visible===false) {
        magicExplosionGroup[e].sprite.x = x || 23;
        magicExplosionGroup[e].sprite.y = y || 23;
        magicExplosionGroup[e].sprite.anims.play(magicExplosionGroup[e].animKey);
        magicExplosionGroup[e].sprite.visible = true;
        magicExplosionGroup[e].speed = speed || 0;
        magicExplosionGroup[e].rotate = rotation || 0;
        break;
      }
    }
  }
};

// new dealer

const blendAsteroidDealer = (index,type,x,y) => {
  if(type==="delete") {
    blendAsteroidGroup[index].sprite.visible=false;
    blendAsteroidGroup[index].explodeStart=true;
  } else if(type==="add") {
    for(let b in blendAsteroidGroup) {
      if(blendAsteroidGroup[b].sprite.visible===false) {
        blendAsteroidGroup[b].sprite.x = x || 23;
        blendAsteroidGroup[b].sprite.y = y || 23;
        blendAsteroidGroup[b].power = blendAsteroidGroup[b].maxPower || 2;
        blendAsteroidGroup[b].sprite.visible = true;
        blendAsteroidGroup[b].explodeStart = false;
        blendAsteroidGroupExplosion[b].visible = false;
        blendAsteroidGroupExplosion[b].anims.play(blendAsteroidGroup[b].animKey+'Explode');
        break;
      }
    }
  }
}

const playerActionDealer = (actionBtn) => {
  if(actionBtn==="left") {
    playerShip.x += -1.5;
    HUD.fuel -= 0.0015;
  } else if(actionBtn==="right") {
    playerShip.x += 1.5;
    HUD.fuel -= 0.0015;
  } else if(actionBtn==="down") {
    playerShip.y += 1;
    HUD.fuel -= 0.001;
  } else if(actionBtn==="up") {
    playerShip.y += -1;
    HUD.fuel -= 0.001;
  } else if(actionBtn==="fire") {
    if(HUD.shootPower>=0 && HUD.shootPower!=1) {
    bulletDealer(0,"add",playerShip.x,playerShip.y);
    }
    if(HUD.shootPower>=1) {
      bulletDealer(0,"add",playerShip.x,playerShip.y,2,.15);
      bulletDealer(0,"add",playerShip.x,playerShip.y,2,-.15);
    } 
    if(HUD.shootPower>=3) {
      bulletDealer(0,"add",playerShip.x,playerShip.y,2,.3);
      bulletDealer(0,"add",playerShip.x,playerShip.y,2,-.3);
    }  
  }
};

// 
// end of dealers here
// 

const bulletCollision = (bulletNr,obj) => {
  bulletDealer(bulletNr,"delete");
  obj.power--;
  if(obj.power<=0) {
    // obj.sprite.visible = false;
    obj.sprite.anims.play(obj.animKey);
    obj.playStart = true;
    explosionDealer(-1,"add",obj.sprite.x,obj.sprite.y,obj.speed,obj.rotate);
    addPointsToScore(obj.maxPower*25);
    if( Math.random() < gOc.bonus.powerFuelDrop ) {
      bonusDealer(0,"add",obj.sprite.x,obj.sprite.y);
    } else if( Math.random() < gOc.bonus.coinDrop) {
      bonusDealer(-1, "coin", obj.sprite.x, obj.sprite.y, obj.speed);
    }
  }
};

const blendAsteroidBulletCollision = (bulletNr, blendNr, blendAst) => {
  bulletDealer(bulletNr,"delete");
  blendAsteroidGroup[blendNr].power--;
  if(blendAsteroidGroup[blendNr].power<=0) {
    blendAsteroidGroup[blendNr].sprite.visible = false;
    blendAsteroidGroupExplosion[blendNr].x = blendAsteroidGroup[blendNr].sprite.x;
    blendAsteroidGroupExplosion[blendNr].y = blendAsteroidGroup[blendNr].sprite.y;
    blendAsteroidGroupExplosion[blendNr].visible = true;
    blendAsteroidGroupExplosion[blendNr].anims.play(blendAsteroidGroup[blendNr].animKey+"Explode",0);
    addPointsToScore(blendAsteroidGroup[blendNr].maxPower*25);
    if( Math.random() < gOc.bonus.powerFuelDrop ) {
      bonusDealer(0,"add",blendAsteroidGroup[blendNr].sprite.x,blendAsteroidGroup[blendNr].sprite.y);
    } else if( Math.random() < gOc.bonus.coinDrop) {
      bonusDealer(-1, "coin", blendAsteroidGroup[blendNr].sprite.x, blendAsteroidGroup[blendNr].sprite.y, blendAsteroidGroup[blendNr].speed);
    }
  }
};

const quadCollision = (bulletNr,quad) => {
  bulletDealer(bulletNr,"delete");
  quad.power--;
  if(quad.power<=0) {
    if( Math.random() >= gOc.bonus.powerFuelDrop ) {
      bonusDealer(-1,"add",quad.sprite.x,quad.sprite.y);
    }
    quad.sprite.destroy();
    explosionDealer(-1,"add",quad.sprite.x,quad.sprite.y,quad.speed,quad.rotate);
    addPointsToScore(100);
  }
};

// all the bonus goes here
const shipCollectBonus = (bonusNr) => {
  // console.dir(bonusGroup[bonusNr]);
  if(bonusGroup[bonusNr].fuelorpower==="powerUp") {
    addPowerToScore(1);
  } else if(bonusGroup[bonusNr].fuelorpower==="fuelUp") {
    addFuelToScore(0.5);
  }
  magicExplosionDealer(-1,"add",bonusGroup[bonusNr].sprite.x,bonusGroup[bonusNr].sprite.y);
  bonusDealer(bonusNr,"delete");
};

const shipCollectCoin = (coinNr) => {
  HUD.shootPower = coinPool[coinNr].coinVal;
  coinPool[coinNr].sprite.visible = false;
  magicExplosionDealer(-1,"add",coinPool[coinNr].sprite.x,coinPool[coinNr].sprite.y);
};

const addPointsToScore = (pointamount) => {
  HUD.score += pointamount;
  HUD.textScore.setText("score : "+parseInt(HUD.score));
};

const addFuelToScore = (pointamount) => {
  HUD.fuel += pointamount;
  HUD.textFuel.setText("fuel  : "+parseInt(HUD.fuel));
};

const addPowerToScore = (pointamount) => {
  HUD.power += pointamount;
  HUD.textPower.setText("power : "+parseInt(HUD.power));
};

// ship against the enemies
const shipCollidedEnemy = (enemyObj) => {
  // 
  if(HUD.power>=enemyObj.power) {
    explosionDealer(-1,"add",enemyObj.sprite.x,enemyObj.sprite.y);
    addPowerToScore(enemyObj.power*-1);
    enemyObj.sprite.visible = false;
  } else if(HUD.power<enemyObj.power) {
    explosionDealer(-1,"add",enemyObj.sprite.x,enemyObj.sprite.y);
    enemyObj.power -= HUD.power;
    overendout('lowPower');
  }
};


const overendout = (msg) => {
  console.log("game over with "+msg);
  let ex = playerShip.x;
  let ey = playerShip.y;
  explosionDealer(-1,"add",ex+15,ey,0,0);
  explosionDealer(-1,"add",ex+15,ey-15,0,0);
  explosionDealer(-1,"add",ex+15,ey+5,0,0);
  // todo: need to draw the exploding ship
  playerShip.active = false;
  HUD.power = 0;
  HUD.fuel = 0;
  game.scene.start("gameOver");
};


const enemyWaveDealer = (action, obj) => {
  if(action==="add") {
    if(enemyWaveWaiter.indexOf(obj)!=-1) return;
    enemyWaveWaiter.push(obj);
    obj.sprite.visible = false;
  } else if(action==="spawn") {
    if(enemyDealerState.last>Date.now()-gOc.startVars.nextShoot) return;
    enemyDealerState.last = Date.now();
    if(enemyWaveWaiter.length===0) return; // enemyWaveDealer('collect');
    let neoCor = getSomeRandomCoordinate();   
    let goFly = enemyWaveWaiter.shift();
    goFly.sprite.x = neoCor.x;
    goFly.sprite.y = neoCor.y;
    goFly.sprite.visible = true;
    goFly.power = goFly.maxPower;
  } else if(action==="collect") {
    console.log("maybe collect some dead enemys");
  }
};

const getSomeRandomCoordinate = () => {
  if(enemyDealerState.rndList.length===0) {
    const oneH = config.height/10;
    for(let i=0;i<=10;i++) {
      let nX = config.width + ( (config.width/42) * Math.random() );
      let nY = ( oneH * i) * Math.random();    
      enemyDealerState.rndList.push({r:(Math.random()*1000*1000),x:nX,y:nY});
    }
    enemyDealerState.rndList.sort( (a,b) => { return a.r - b.r } );
  }
  
  const toRe = enemyDealerState.rndList.shift();
  return {x:toRe.x, y:toRe.y};
};