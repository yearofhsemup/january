const restPlayData = () => {
  HUD.score = 0;
  HUD.fuel = gOc.startVars.fuel;
  HUD.power = gOc.startVars.power;
  HUD.shootPower = gOc.startVars.shootPower;
};

const createFramesFromTo = (key,start,end) => {
  let nFrames = [];
  for(let c=start;c<=end;c++) {
    nFrames.push({key,frame:c});
  }
  return nFrames;
};

const createFramesToFrom = (key, start, end) => {
  let nFrames = [];
  for(let c=start;c>=end;c--) {
    nFrames.push({key,frame:c});
  }
  return nFrames;
};

const createFramesByList = (key, list) => {
  let nFrames = [];
  for(let c in list) {
    nFrames.push({key,frame:list[c]});
  }
  return nFrames;
};