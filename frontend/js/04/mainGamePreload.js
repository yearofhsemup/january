function mainGamePreload() {
  this.load.spritesheet('ship', 'assets/sprites/Jan_newSmall-PlayerShip.png', { frameWidth: 48, frameHeight: 24 });
  this.load.spritesheet('fireball', 'assets/sprites/Jan_Fireball.png', { frameWidth: 16, frameHeight: 8 });

  this.load.spritesheet('laser', 'assets/sprites/Jan_LaserShoot.png', { frameWidth: 3, frameHeight: 3 });
  this.load.image('firebullet', 'assets/sprites/Jan_FireBullet.png');  
  
  this.load.spritesheet('powerUp', 'assets/sprites/Jan_PowerUpBowl.png', { frameWidth: 8, frameHeight: 8 });
  this.load.spritesheet('fuelUp', 'assets/sprites/Jan_FuelUpBowl.png', { frameWidth: 8, frameHeight: 8 });

  this.load.spritesheet('quadEvil', 'assets/sprites/Jan_QuadEvil_01.png', { frameWidth: 16, frameHeight: 16 });
  this.load.spritesheet('asteroid01', 'assets/sprites/Jan_Asteroid.png', { frameWidth: 32, frameHeight: 24 });
  this.load.spritesheet('asteroid02', 'assets/sprites/Jan_Asteroid_klein.png', { frameWidth: 16, frameHeight: 16 });
  this.load.spritesheet('asteroid03', 'assets/sprites/Jan_Asteroid_03.png', { frameWidth: 16, frameHeight: 16 });
  
  this.load.spritesheet('explosion01', 'assets/sprites/Jan_Explosion_101.png', { frameWidth: 32, frameHeight: 32 });
  this.load.spritesheet('magicExplosion01', 'assets/sprites/Jan_MagicExplode.png', { frameWidth: 32, frameHeight: 32 });
  this.load.spritesheet('coin123', 'assets/sprites/Jan_Coin_one23.png', { frameWidth: 16, frameHeight: 16 });

  // new Asteroidz 
  this.load.spritesheet('blendAsteroid01', 'assets/sprites/Jan_blend_Asteroid_01.png', { frameWidth: 32, frameHeight: 32 });
  this.load.spritesheet('blendAsteroid02', 'assets/sprites/Jan_blend_Asteroid_02.png', { frameWidth: 32, frameHeight: 32 });
  this.load.spritesheet('blendAsteroid03', 'assets/sprites/Jan_blend_green-Asteroid_01.png', { frameWidth: 32, frameHeight: 32 });
  this.load.spritesheet('blendAsteroid04', 'assets/sprites/Jan_blend_green-Asteroid_02.png', { frameWidth: 32, frameHeight: 32 });

  // new enemyz
  this.load.spritesheet('quadEvil02', 'assets/sprites/Jan_QuadEvil_02.png',{ frameWidth: 32, frameHeight: 32 });
}