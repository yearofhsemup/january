function mainGameUpdate() {
  // ship movement
  if(HUD.fuel>0 && !GAMEOVER.mainGameStop) {
    if (CURSORS.left.isDown) {
      playerActionDealer('left');
      // playerShip.x += -1.5;
      // HUD.fuel -= 0.0015;
    } else if (CURSORS.right.isDown) {
      playerActionDealer('right');
      // playerShip.x += 1.5;
      // HUD.fuel -= 0.0015;
    }
    if(CURSORS.down.isDown) {
      playerActionDealer('down');
      // playerShip.y += 1;
      // HUD.fuel -= 0.001;
    } else if(CURSORS.up.isDown) {
      playerActionDealer('up');
      // playerShip.y += -1;
      // HUD.fuel -= 0.001;
    }
    if(GAMEPAD||false) {
      if (GAMEPAD.left||GAMEPAD.buttons[14].pressed) {
        playerActionDealer('left');
      } else if (GAMEPAD.right||GAMEPAD.buttons[15].pressed) {
        playerActionDealer('right');
      }

      if (GAMEPAD.up||GAMEPAD.buttons[12].pressed) {
        playerActionDealer('up');
      } else if (GAMEPAD.down||GAMEPAD.buttons[13].pressed) {
        playerActionDealer('down');
      }

      if(GAMEPAD.Y) {
        AUTOFIRE = (AUTOFIRE||false)? true : false;
      }
    }
  }
  if(!GAMEOVER.mainGameStop && !GAMEOVER.mainGamePause) {  // ship word bounce
    let sX = playerShip.x;
    let sY = playerShip.y;
    let rH = playerShip.width/2;
    let rV = playerShip.height/2;
    if(sX-rH<=0) {
      playerShip.x = rH;
    } else if(sX+rH>=config.width) {
      playerShip.x = config.width-rH;
    }

    if(sY-rV<=0) {
      playerShip.y = rV;
    } else if(sY+rV>=config.height) {
      playerShip.y = config.height-rV;
    }
    
    if( LASTBULLET+500 <= Date.now()  ) {
      // if(HUD.shootPower>=0 && HUD.shootPower!=1) {
      //   bulletDealer(0,"add",playerShip.x,playerShip.y);
      // }
      // if(HUD.shootPower>=1 || HUD.shootPower===2) {
      //   bulletDealer(0,"add",playerShip.x,playerShip.y,2,.15);
      //   bulletDealer(0,"add",playerShip.x,playerShip.y,2,-.15);
      // } 
      // if(HUD.shootPower>=3) {
      //   bulletDealer(0,"add",playerShip.x,playerShip.y,2,.3);
      //   bulletDealer(0,"add",playerShip.x,playerShip.y,2,-.3);
      // }
      if(FIREBUTTON.isDown || AUTOFIRE) { 
        playerActionDealer('fire');
        LASTBULLET = Date.now();  
      }

      if(GAMEPAD||false) {
        if(GAMEPAD.A) {
          playerActionDealer('fire');
          LASTBULLET = Date.now();
        }
      }
    }

    HUD.nextFuelCount++;
    if(HUD.power<=1 && HUD.nextPowerCount>HUD.nextPower) {
      HUD.nextPowerCount = 0;
      addPowerToScore(.5);
    }
    if(HUD.fuel<=1 && HUD.nextFuelCount>HUD.nextFuel) {
      HUD.nextFuelCount = 0;
      addFuelToScore(1);
    }
    if(HUD.nextPowerCount%100 || HUD.nextFuelCount%100) {
      addFuelToScore(0);
      addPowerToScore(0);
    }
  }
  // move explosion
  // for(let e in explosionGroup) {
  //   if(!explosionGroup[e].sprite.visible) continue;
  //   explosionGroup[e].sprite.x += asteroidGroup[e].speed;
  //   explosionGroup[e].sprite.rotation += asteroidGroup[e].rotate;
  // }
  // // magicExplosionAnim01
  // // move magic explosion
  // for(let e in magicExplosionGroup) {
  //   if(!magicExplosionGroup[e].sprite.visible) continue;
  //   magicExplosionGroup[e].sprite.x += asteroidGroup[e].speed;
  //   magicExplosionGroup[e].sprite.rotation += asteroidGroup[e].rotate;
  // }

  // move asteroidz
  // for(let a in asteroidGroup) {
  //   if( asteroidGroup[a].sprite.visible && (asteroidGroup[a].sprite.x <= (asteroidGroup[a].sprite.width/2)*-1) ) {
  //     asteroidDealer(a,"delete");
  //   } else if(!asteroidGroup[a].sprite.anims.isPlaying && asteroidGroup[a].playStart) {
  //     asteroidDealer(a,"delete");
  //   } else if(!asteroidGroup[a].sprite.visible) {
  //     asteroidDealer(-1, "add", config.width+asteroidGroup[a].sprite.width, Math.random()*config.height);
  //   } else {
  //     asteroidGroup[a].sprite.rotation += asteroidGroup[a].rotate;
  //     asteroidGroup[a].sprite.x += asteroidGroup[a].speed;
  //   }
  // }

  // move new asteroidz
  for(let b in blendAsteroidGroup) {
    if( blendAsteroidGroup[b].sprite.visible && (blendAsteroidGroup[b].sprite.x <= (blendAsteroidGroup[b].sprite.width/2)*-1) ) {
      blendAsteroidDealer(b,"delete");
    } else if(blendAsteroidGroup[b].explodeStart && !blendAsteroidGroupExplosion[b].anims.isPlaying) {
      // blendAsteroidDealer(b,"delete");
      enemyWaveDealer('add', blendAsteroidGroup[b]);
      blendAsteroidGroupExplosion[b].visible = false;
    } else if(!blendAsteroidGroup[b].sprite.visible && !blendAsteroidGroupExplosion[b].visible) {
      // blendAsteroidDealer(-1, "add", config.width+blendAsteroidGroup[b].sprite.width, Math.random()*config.height);
      enemyWaveDealer('add', blendAsteroidGroup[b]);
    } else {      
      blendAsteroidGroup[b].sprite.x += blendAsteroidGroup[b].speed;
    }
  }


  // move bullets
  for(let b in bulletGroup) {
    if( !bulletGroup[b].sprite.visible ) continue;
    bulletGroup[b].sprite.x += bulletGroup[b].speedX;
    bulletGroup[b].sprite.y += bulletGroup[b].speedY;
    if(bulletGroup[b].sprite.x-bulletGroup[b].sprite.width>config.width) {
      bulletDealer(b,"delete")
    }
  }

  // move evil quadz
  quadAttacker.wavePos += quadAttacker.speedW;
  quadAttacker.X += quadAttacker.speedX;
  for(let q in quadAttacker.quadz) {
    quadAttacker.quadz[q].sprite.x = quadAttacker.X + (q*quadAttacker.frq);
    quadAttacker.quadz[q].sprite.y = quadAttacker.Y + ( Math.sin( ( (2*Math.PI/quadAttacker.qCounts) * q) + quadAttacker.wavePos ) * quadAttacker.amp ); 
  }

  // move evil quadz 2
  // enemyz.quadEvil02.X += enemyz.quadEvil02.speedX;
  for(let qf in enemyz.quadEvil02.enemyz) {
    enemyz.quadEvil02.enemyz[qf].sprite.x += enemyz.quadEvil02.enemyz[qf].speedX;
    enemyz.quadEvil02.enemyz[qf].sprite.y += enemyz.quadEvil02.enemyz[qf].speedY;
  }
  
  // move bonus boubles
  for(let qb in bonusGroup) {
    if(!bonusGroup[qb].sprite.visible) continue;
    if(bonusGroup[qb].sprite.y-bonusGroup[qb].sprite.height>config.height) {
      bonusGroup[qb].sprite.visible = false;
      continue;
    }
    let nx = (bonusGroup[qb].snowF==="left")? bonusGroup[qb].snowS*-1 : bonusGroup[qb].snowS; 
    bonusGroup[qb].sprite.x += nx;
    bonusGroup[qb].sprite.y += bonusGroup[qb].snowS;
    if(Math.random()<.03) {
      bonusGroup[qb].snowF = (bonusGroup[qb].snowF==="left")? "right" : "left";
      bonusGroup[qb].sprite.y += bonusGroup[qb].snowS;
    }
  }


  // collision defraxktion
  for(let b in bulletGroup) {
    if( !bulletGroup[b].sprite.visible ) continue;
    // check asteroidGroup
    // for(let a in asteroidGroup) {
    //   if( !asteroidGroup[a].sprite.visible || asteroidGroup[a].power<=0 ) continue;
    //   if( hasCollided(asteroidGroup[a].sprite,bulletGroup[b].sprite) ) {
    //     bulletCollision(b,asteroidGroup[a]);
    //   }
    // }

    for(let ba in blendAsteroidGroup) {
      if( !blendAsteroidGroup[ba].sprite.visible || blendAsteroidGroup[ba].power<=0) continue;
      if( hasCollided(blendAsteroidGroup[ba].sprite,bulletGroup[b].sprite) ) {
        blendAsteroidBulletCollision(b,ba);
      }
    }
    // check evil quad attacker
    let killAllCount = 0;
    for(let q in quadAttacker.quadz) {
      if(!quadAttacker.quadz[q].sprite.visible) {
        killAllCount++;
        continue;
      }
      if(hasCollided(quadAttacker.quadz[q].sprite,bulletGroup[b].sprite)) quadCollision(b,quadAttacker.quadz[q]);
    }
    if(quadAttacker.qCounts<killAllCount) {
      createAttacker(this,config.width+100, quadAttacker.amp+(Math.random()*(quadAttacker.amp/2)));
    }
  }

  // collision and interaction with the ship
  if(playerShip.active) {
    for(let b in bonusGroup) {
      if( bonusGroup[b].sprite.visible && hasCollided(bonusGroup[b].sprite,playerShip)) {
        shipCollectBonus(b,playerShip);
      }
    }

    for(let a in blendAsteroidGroup) { 
      if(!blendAsteroidGroup[a].sprite.visible) continue;
      if(hasCollided(blendAsteroidGroup[a].sprite,playerShip)) shipCollidedEnemy(blendAsteroidGroup[a]);
    }
    for(let q in quadAttacker.quadz) {
      if(!quadAttacker.quadz[q].sprite.visible) continue;
      if(hasCollided(quadAttacker.quadz[q].sprite,playerShip)) shipCollidedEnemy(quadAttacker.quadz[q]);
    }

    for(let c in coinPool) {
      if(!coinPool[c].sprite.visible) continue;
      if(hasCollided(coinPool[c].sprite,playerShip)) {
        shipCollectCoin(c);
      } else {
        coinPool[c].sprite.x += coinPool[c].speed;
      }
    }
  }
  enemyWaveDealer("spawn");
}