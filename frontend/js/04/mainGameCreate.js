function mainGameCreate() {
  HUD.activeScene = 'mainGame';
  game.config.backgroundColor = "#232342";

  CURSORS = this.input.keyboard.createCursorKeys();
  FIREBUTTON = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.X);
  this.input.keyboard.on('keyup', event => {
    if(event.keyCode === 65) { // press a
      AUTOFIRE = (AUTOFIRE||false)? false : true;
    } else if(event.keyCode === 27) {// press esc
      overendout("kill me !!1!!");
    } 
  });

  this.input.on('pointermove', pointer => {
    if(pointer.isDown && !this.scene.isActive('gameOver')) {
      playerShip.x += pointer.x - HUD.pointer.lastX;
      playerShip.y += pointer.y - HUD.pointer.lastY;
      HUD.pointer.lastX = pointer.x;
      HUD.pointer.lastY = pointer.y;
    }
  });

  this.input.on('pointerdown', pointer => {
    HUD.pointer.lastX = pointer.x;
    HUD.pointer.lastY = pointer.y;
  });

  this.input.gamepad.once('down', function(pad, button, index) {
    GAMEPAD = pad;
  }, this);

  if(!fireballAnim) {
    animConfigs.fireball.frames = this.anims.generateFrameNumbers('fireball');
    fireballAnim = this.anims.create(animConfigs.fireball);
  }
  if(!powerUpAnim) {
    animConfigs.powerUp.frames = this.anims.generateFrameNumbers('powerUp');
    powerUpAnim = this.anims.create(animConfigs.powerUp);
  }
  if(!fuelUpAnim) {
    animConfigs.fuelUp.frames = this.anims.generateFrameNumbers('fuelUp');
    fuelUpAnim = this.anims.create(animConfigs.fuelUp);
  }
  if(!quadEvilAnim) {
    animConfigs.quadEvil.frames = this.anims.generateFrameNumbers('quadEvil');  
    quadEvilAnim = this.anims.create(animConfigs.quadEvil);
  }
  if(!asteroidAnim01) {
    animConfigs.asteroid01.frames = this.anims.generateFrameNumbers('asteroid01');
    asteroidAnim01 = this.anims.create(animConfigs.asteroid01);
  }
  if(!asteroidAnim02) {
    animConfigs.asteroid02.frames = this.anims.generateFrameNumbers('asteroid02');
    asteroidAnim02 = this.anims.create(animConfigs.asteroid02);
  }
  if(!asteroidAnim03) {
    animConfigs.asteroid03.frames = this.anims.generateFrameNumbers('asteroid03');
    asteroidAnim03 = this.anims.create(animConfigs.asteroid03);
  }
  if(!explosionAnim01) {
    animConfigs.explosion01.frames = this.anims.generateFrameNumbers('explosion01');
    explosionAnim01 = this.anims.create(animConfigs.explosion01);
  }

  if(!magicExplosionAnim01) {
    animConfigs.magicExplosion01.frames = this.anims.generateFrameNumbers('magicExplosion01');
    magicExplosionAnim01 = this.anims.create(animConfigs.magicExplosion01);
  }

  if(!coinAnim01) {
    coinAnim01 = this.anims.create(animConfigs.coin1);
  }
  if(!coinAnim02) {
    coinAnim02 = this.anims.create(animConfigs.coin2);
  }
  if(!coinAnim03) {
    coinAnim03 = this.anims.create(animConfigs.coin3);
  }

  // new Asteroidz

  if(!blendAsteroid01Anim) {
    blendAsteroid01Anim = this.anims.create(animConfigs.blendAsteroid01);
  }

  if(!blendAsteroid01Explosion) {
    blendAsteroid01Explosion = this.anims.create(animConfigs.blendAsteroid01Explode);
  }

  if(!blendAsteroid02Anim) {
    blendAsteroid02Anim = this.anims.create(animConfigs.blendAsteroid02);
  }

  if(!blendAsteroid02Explosion) {
    blendAsteroid02Explosion = this.anims.create(animConfigs.blendAsteroid02Explode);
  }

  if(!blendAsteroid04Anim) {
    blendAsteroid04Anim = this.anims.create(animConfigs.blendAsteroid04);
  }

  if(!blendAsteroid04Explosion) {
    blendAsteroid04Explosion = this.anims.create(animConfigs.blendAsteroid04Explode);
  }

  if(!blendAsteroid03Anim) {
    blendAsteroid03Anim = this.anims.create(animConfigs.blendAsteroid03);
  }

  if(!blendAsteroid03Explosion) {
    blendAsteroid03Explosion = this.anims.create(animConfigs.blendAsteroid03Explode);
  }

  if(!quadEvil02DownAnim) {
    quadEvil02DownAnim = this.anims.create(animConfigs.quadEvil02Down);
  }

  if(!quadEvil02RightAnim) {
    quadEvil02RightAnim = this.anims.create(animConfigs.quadEvil02Right);
  }

  if(!quadEvil02LeftAnim) {
    quadEvil02LeftAnim = this.anims.create(animConfigs.quadEvil02Left);
  }

  if(!quadEvil02UpAnim) {
    quadEvil02UpAnim = this.anims.create(animConfigs.quadEvil02Up);
  }
  // let testCoin1 = this.add.sprite(240,120, 'coin123');
  // testCoin1.anims.load('coinRotate1');
  // testCoin1.anims.play('coinRotate1');
  // let testCoin2 = this.add.sprite(250,130, 'coin123');
  // testCoin2.anims.load('coinRotate2');
  // testCoin2.anims.play('coinRotate2');
  // let testCoin3 = this.add.sprite(260,135, 'coin123');
  // testCoin3.anims.load('coinRotate3');
  // testCoin3.anims.play('coinRotate3');

  // create bonusGroup pool
  for(let qb=0;qb<gOc.startVars.objBonusPowFu;qb++) {
    let fuelorpower = ( gOc.bonus.powerOverFuel < Math.random() )? 'powerUp' : 'fuelUp';
    bonusGroup[qb] = {snowF: "left",snowS:0.1,snowP:0,snowM:2,fuelorpower};
    bonusGroup[qb].sprite = this.add.sprite(0, 0, fuelorpower);
    bonusGroup[qb].sprite.anims.load(fuelorpower+'Rotate');
    bonusGroup[qb].sprite.anims.play(fuelorpower+'Rotate');
    bonusGroup[qb].sprite.visible = false;
  }

  // create coin pool
  for(let c=0;c<=gOc.startVars.objCoins;c++) {
    let coinVal = parseInt(Math.random()*3)+1;
    coinPool[c] = {animKey:'coinRotate'+coinVal,coinVal:coinVal,speed:0,rotate:0};
    coinPool[c].sprite = this.add.sprite(0,0, 'coin123');
    coinPool[c].sprite.anims.load('coinRotate'+coinVal);
    coinPool[c].sprite.anims.play('coinRotate'+coinVal);
    coinPool[c].sprite.visible = false;
  }

  // create explosion pool
  for(let e=0;e<=gOc.startVars.objExplosions;e++) {
    explosionGroup[e] = {start:false,animKey:'force01',speed:0,rotate:0};
    explosionGroup[e].sprite = this.add.sprite(0,0, 'explosion01');
    explosionGroup[e].sprite.setAngle(Math.random()*360);
    explosionGroup[e].sprite.anims.load('force01');
    explosionGroup[e].sprite.anims.play('force01');
    explosionGroup[e].sprite.visible = false;
  }

  // create magic explosion pool
  for(let e=0;e<=gOc.startVars.objMagicExplosions;e++) {
    magicExplosionGroup[e] = {start:false,animKey:'curse01',speed:0,rotate:0};
    magicExplosionGroup[e].sprite = this.add.sprite(0, 0, 'magicExplosion01');
    magicExplosionGroup[e].sprite.setAngle(Math.random()*360);
    magicExplosionGroup[e].sprite.anims.load('curse01');
    magicExplosionGroup[e].sprite.anims.play('curse01');
    magicExplosionGroup[e].sprite.visible = false;
  }




  // create asteroid field
  // todo:
  //      better asteroid dealer

  // for(let a=0;a<gOc.startVars.objAsteroids;a++) {
  //   let nY = Math.random()*config.height;
  //   let nX = (Math.random()*(config.width*2))+config.width/4;
  //   let aT = parseInt(Math.random()*3)+1;
  //   let aA = Math.random()*360;
  //   let aS = (Math.random()*0.4)+0.1;
  //   let aR = aS/10;
  //   asteroidGroup[a] = {
  //     animKey: 'explode0'+aT,
  //     speed: aS*-1,
  //     rotate: aR*-1,
  //     sprite: {},
  //     power: aT,
  //     maxPower: aT,
  //     playStart: false
  //   };
  //   asteroidGroup[a].sprite = this.add.sprite(nX,nY, 'asteroid0'+aT);
  //   asteroidGroup[a].sprite.setAngle(aA);

  //   asteroidGroup[a].collidedRadius = (asteroidGroup[a].sprite.width+asteroidGroup[a].sprite.height)/2;
  // }

  // new asteroid create section
  blendAsteroidGroup = [];
  blendAsteroidGroupExplosion = [];
  for(let na=0; na < gOc.startVars.objAsteroids ; na++) {
    let nY = Math.random()*config.height;
    let nX = (Math.random()*(config.width*2))+config.width/4;
    let aT = 'blendAsteroid0'+(parseInt(Math.random()*4)+1);
    let mP = parseInt(Math.random()*4)+1;

    blendAsteroidGroup[na] = {
      animKey:aT,
      speed: ((Math.random()*0.4)+0.1)*-1, 
      power: mP, 
      maxPower: mP, 
      sprite:{},
      explodeStart: false
    };
    blendAsteroidGroup[na].sprite = this.add.sprite(nX,nY, aT);
    blendAsteroidGroupExplosion[na] = this.add.sprite(nX,nY, aT);
    blendAsteroidGroup[na].sprite.anims.load(aT+'Fly');
    blendAsteroidGroup[na].sprite.anims.play(aT+'Fly');
    blendAsteroidGroupExplosion[na].anims.load(aT+'Explode');
    // blendAsteroidGroupExplosion[na].anims.play('blendAsteroid01Explode');
    blendAsteroidGroupExplosion[na].visible = false;
  }

  this,enemyz.quadEvil02 = createEnemyGroup(this,config.width/2,config.height/2);

  // // create quadEvil 02 
  // enemyz.quadEvil02 = {};
  // for(let qa=0; qa < gOc.startVars.objFormEnemy; qa++) {
  //   enemyz.quadEvil02[qa] = {
  //     activeKey: 'quadEvil02Down',
  //     power: 3,
  //     sprite: {},
  //     explodeStart: false
  //   };
  //   enemyz.quadEvil02[qa].sprite = this.add.sprite( (config.width/2)+(32*qa) ,config.height/2, 'quadEvil02');
  //   enemyz.quadEvil02[qa].sprite.anims.load('quadEvil02Down');
  //   enemyz.quadEvil02[qa].sprite.anims.play('quadEvil02Down');
  // }


  // create bullet pool
  bulletGroup = [];
  for(let b=0;b<50;b++) {
    bulletGroup[b] = {speedX:2,speedY:0,power:1};
    bulletGroup[b].sprite = this.add.image(-23,-42, 'firebullet');
    bulletGroup[b].sprite.visible=false;    
  }

  createAttacker(this,600,150);
  if(!shipAnim) {
    // animConfigs.ship.frames = this.anims.generateFrameNumbers('ship');
    shipAnim = this.anims.create(animConfigs.ship);
  }
  playerShip = this.add.sprite(400,160, 'ship');
  playerShip.anims.load('shipFly');
  playerShip.anims.play('shipFly');

  HUD.textScore = this.add.text(16, 16, 'score : '+HUD.score, { fontSize: '16px', fill: '#FFF' });
  HUD.textPower = this.add.text(16, 32, 'power : '+HUD.power, { fontSize: '16px', fill: '#FFF' });
  HUD.textFuel = this.add.text(16, 48, 'fuel  : '+HUD.fuel, { fontSize: '16px', fill: '#FFF' });
  
}


const createAttacker = (that,x,y) => {
  // create quad evil attacker
  // todo:
  //      make attacker sporn able nx
  quadAttacker = {
    quadz: [],
    animKey: 'quadEvilRotate',
    X: x,
    Y: y,
    amp: 60,
    frq: 30,
    qCounts: 10,
    bonusCount: 0,
    speedX: -.1,
    speedY: 0,
    speedW: -0.03,
    wavePos: 0,
    powerStart: 4,    
  };
  for(let q=0,ql=quadAttacker.qCounts;q<=ql;q++) {
    // quadAttacker.quadz[q] = this.add.sprite(, , 'quadEvil');
    let sx = quadAttacker.X + (q*quadAttacker.frq);
    // let sy = quadAttacker.Y + ( Math.sin( ((2*Math.PI/quadAttacker.qCounts) * q)-quadAttacker.wavePos ) * quadAttacker.amp ); 
    let sy = quadAttacker.Y;
    quadAttacker.quadz[q] = {power:quadAttacker.powerStart,speed:quadAttacker.speedX,rotate:0.1,sprite:{}};
    quadAttacker.quadz[q].sprite = that.add.sprite(sx,sy, 'quadEvil');
    quadAttacker.quadz[q].sprite.anims.load(quadAttacker.animKey);
    quadAttacker.quadz[q].sprite.anims.play(quadAttacker.animKey);
    
  }
}

const createEnemyGroup = (that,x,y) => {
  let obj = {
    enemyz: [],
    X: x,
    Y: y,
    amp: 60,
    frq: 30,
    qCounts: 10,
    bonusCount: 0,
    speedX: -.1,
    speedY: 0,
    speedW: -0.03,
    wavePos: 0,
    maxPower: 4,
  };
  let radL = [{anim:'Right',mX:0.1,mY:0},{anim:'Down',mX:0,mY:0.1},{anim:'Left',mX:-0.1,mY:0},{anim:'Up',mX:0,mY:-0.1}];
  for(let e=0; e <= gOc.startVars.objFormEnemy; e++) {
      // create quadEvil 02 
    let r = parseInt(Math.random()*4);
    obj.enemyz[e] = {
      activeKey: "quadEvil02" + radL[r].anim,
      power: 3,
      maxPower: 4,
      sprite: {},
      explodeStart: false,
      speedX: radL[r].mX,
      speedY: radL[r].mY,
    };
    obj.enemyz[e].sprite = that.add.sprite( Math.random()*config.width ,Math.random()*config.height, 'quadEvil02');
    obj.enemyz[e].sprite.anims.load(obj.enemyz[e].activeKey);
    obj.enemyz[e].sprite.anims.play(obj.enemyz[e].activeKey);
    enemyWaveDealer("add",obj.enemyz[e]);
  }
  return obj;
};