const ZOOM = 2;
const config = {
    type: Phaser.AUTO,
    width: 800,
    height: 300,
    zoom: ZOOM,
    pixelArt: true,
    input: {
      gamepad: true
    },
    // physics: {
    //   default: 'arcade',
    //   arcade: {
    //     gravity: { y: 300 },
    //     debug: false
    //   }
    // },
    scene: [
      { key: 'mainMenu', preload: mainMenuPreload, create: mainMenuCreate, update: mainMenuUpdate },
      { key: 'mainGame', active: false,  preload: mainGamePreload, create: mainGameCreate, update: mainGameUpdate },
      { key: 'gameOver', active: false, preload: gameOverPreload, create: gameOverCreate, update: gameOverUpdate}
      ],
  callbacks: {
    postBoot: function (game) {
      var config = game.config;
      var style = game.canvas.style;
      style.width = (ZOOM * config.width) + 'px';
      style.height = (ZOOM * config.height) + 'px';
    }
  }
};

let animConfigs = {
  laser: {
    key: 'laserFly',
    frames: 2,
    frameRate: 4,
    // yoyo: false,
    repeat: -1
  },
  powerUp: {
    key: 'powerUpRotate',
    frames: 7,
    frameRate: 6,
    // yoyo: false,
    repeat: -1
  },
  fuelUp: {
    key: 'fuelUpRotate',
    frames: 7,
    frameRate: 6,
    // yoyo: false,
    repeat: -1
  },
  quadEvil: {
    key: 'quadEvilRotate',
    frames: 15,
    frameRate: 9,
    // yoyo: false,
    repeat: -1
  },
  asteroid01: {
    key: 'explode01',
    frames: 6,
    frameRate: 5,
    hideOnComplete: true,
    // yoyo: false,
    repeat: 0
  },
  asteroid02: {
    key: 'explode02',
    frames: 5,
    frameRate: 5,
    hideOnComplete: true,
    // yoyo: false,
    repeat: 0
  },
  asteroid03: {
    key: 'explode03',
    frames: 5,
    frameRate: 5,
    hideOnComplete: true,
    // yoyo: false,
    repeat: 0
  },
  explosion01: {
    key: 'force01',
    frames: 32,
    frameRate: 32,
    hideOnComplete: true,
    // yoyo: false,
    repeat: 0
  },
  magicExplosion01: {
    key: 'curse01',
    frames: 32,
    frameRate: 32,
    hideOnComplete: true,
    // yoyo: false,
    repeat: 0    
  }
};

let HUD = {
  power: 7,
  fuel: 6,
  score: 0,
  ships: 2,
  level: 0,
  shootPower: 0,
  textScore: {},
  textPower: {},
  textFuel: {},
  nextPower: 2000,
  nextFuel: 4223,
  nextPowerCount: 0,
  nextFuelCount: 0,
  activeScene: 'mainMenu',
  pointer: {
    lastX: 0,
    lastY: 0,
    accelerationX: 0,
    accelerationY: 0,
    accelerationMaxX: 1,
    accelerationMaxY: 1,
  }
};

const gOc = {
  bonus: {
    powerFuelDrop: 0.3,
    coinDrop: 0.1,
    powerOverFuel: 0.6,

  },
  startVars: {
    power: 7,
    fuel: 6,
    ships: 2,
    shootPower: 0,
    nextShoot: 500,
    objAsteroids: 30,
    objFormEnemy: 30,
    objBonusPowFu: 20,
    objCoins: 5,
    objExplosions: 40,
    objMagicExplosions: 30
  }
};

let CURSORS = {};
let GAMEPAD = null;
let FIREBUTTON = {};
let AUTOFIRE = true;
let NOTPRELL = 0;

let LASTBULLET = 0;
// let BULLETPOWER = 1;

// main menu
let pointerAnim = {};

let playerShip = {};
let bulletGroup = [];
let bonusGroup = [];
let explosionGroup = [];
let magicExplosionGroup = [];
let asteroidGroup = [];
let quadAttacker = [];
let coinPool = [];


let fireballAnim = null;
let powerUpAnim = null;
let fuelUpAnim = null;
let quadEvilAnim = null;
let asteroidAnim01 = null;
let asteroidAnim02 = null;
let asteroidAnim03 = null;
let explosionAnim01 = null;
let magicExplosionAnim01 = null;
let shipAnim = null;
let coinAnim01 = null;
let coinAnim02 = null;
let coinAnim03 = null;

// let fireballAnim = {};

// config.scene=[{preload: mainMenuPreload,
//         create: mainMenuCreate,
//         update: mainMenuUpdate}];

var game = new Phaser.Game(config);