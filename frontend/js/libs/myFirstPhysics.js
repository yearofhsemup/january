const hasCollided = (obj1,obj2) => {
  return ( 
            (obj1.width/2 + obj2.width/2) >
            Math.sqrt(
                Math.pow( obj1.x - obj2.x ,2) +
                Math.pow( obj1.y - obj2.y ,2)
              )
          );
};